local cleanMapAtServerSave = true

local function serverSave()
	updateGlobalStorage(DailyReward.storages.lastServerSave, os.time())

	if cleanMapAtServerSave then
			cleanMap()
	end
end

local function secondServerSaveWarning()
	Game.broadcastMessage("CLEAN MAP in 1 minutes. Please go to a safe place and wait.", MESSAGE_EVENT_ADVANCE)
	addEvent(serverSave, 60000)
end 
 
local function firstServerSaveWarning()
	Game.broadcastMessage("CLEAN MAP in 3 minutes. Please go to a safe place and wait.", MESSAGE_EVENT_ADVANCE)
	addEvent(secondServerSaveWarning, 120000)
end

function onTime(interval)
	Game.broadcastMessage("CLEAN MAP in 5 minutes. Please go to a safe place and wait.", MESSAGE_EVENT_ADVANCE)
	addEvent(firstServerSaveWarning, 120000)
end
