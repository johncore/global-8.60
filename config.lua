-- Combat settings
-- NOTE: valid values for worldType are: "pvp", "no-pvp" and "pvp-enforced"
worldType = "pvp"
hotkeyAimbotEnabled = true
protectionLevel = 1
killsToRedSkull = 10
killsToBlackSkull = 20
pzLocked = 60 * 1000
removeChargesFromRunes = false
removeChargesFromPotions = true
removeWeaponAmmunition = false
removeWeaponCharges = false
timeToDecreaseFrags = 30 * 60 * 1000
whiteSkullTime = 7 * 60 * 1000
stairJumpExhaustion = 2 * 1000
experienceByKillingPlayers = false
expFromPlayersLevelRange = 75

-- Connection Config
-- NOTE: maxPlayers set to 0 means no limit
ip = "mega.warzona.com"
bindOnlyGlobalAddress = false
loginProtocolPort = 7171
gameProtocolPort = 7172
statusProtocolPort = 7171
maxPlayers = 0
motd = "Bem vindo ao Warzona!"
onePlayerOnlinePerAccount = false
allowClones = false
serverName = "Warzona"
statusTimeout = 5 * 1000
replaceKickOnLogin = true
maxPacketsPerSecond = 35

-- Version Manual
clientVersionMin = 860
clientVersionMax = 860
clientVersionStr = "Only clients with protocol 8.60 allowed!"

-- Deaths
-- NOTE: Leave deathLosePercent as -1 if you want to use the default
-- death penalty formula. For the old formula, set it to 10. For
-- no skill/experience loss, set it to 0.
deathLosePercent = -1

-- Houses
-- NOTE: set housePriceEachSQM to -1 to disable the ingame buy house functionality
housePriceEachSQM = 10000
houseRentPeriod = "never"

-- Item Usage
timeBetweenActions = 800
timeBetweenExActions = 1000

-- Map
-- NOTE: set mapName WITHOUT .otbm at the end
mapName = "warzave"
mapAuthor = "brucesnan"

-- MySQL
mysqlHost = "127.0.0.1"
mysqlUser = "root"
mysqlPass = "senha"
mysqlDatabase = "database"
mysqlPort = 3306
mysqlSock = ""
passwordType = "sha1"


-- Misc.
allowChangeOutfit = true
freePremium = true
kickIdlePlayerAfterMinutes = 15
maxMessageBuffer = 4
emoteSpells = true
classicEquipmentSlots = false
allowWalkthrough = true
classicAttackSpeed = true

-- Rates
-- NOTE: rateExp is not used if you have enabled stages in data/XML/stages.xml
rateExp = 5
rateSkill = 15
rateLoot = 4
rateMagic = 10
rateSpawn = 1

-- Monster rates
rateMonsterHealth = 1.0
rateMonsterAttack = 1.0
rateMonsterDefense = 1.0

-- Monsters
deSpawnRange = 2
deSpawnRadius = 50

-- Stamina
staminaSystem = true

-- Scripts
warnUnsafeScripts = true
convertUnsafeScripts = true

-- Startup
-- NOTE: defaultPriority only works on Windows and sets process
-- priority, valid values are: "normal", "above-normal", "high"
defaultPriority = "high"
startupDatabaseOptimization = true

-- Status server information
ownerName = "Warzona"
ownerEmail = "renan.ulian@gmail.com"
url = "http://warzona.online/"
location = "Brazil"